const card_arr = [
    { src: "vasily_1.jpg"},
    { src: "vasily_2.jpg"},
    { src: "vasily_3.jpg"},
  ];

const about_me = `
Dynamic IT professional with 6+ years of experience, both as a software developer and team coordinator. 
Proficient in Agile methodologies, I excel at planning Scrum sprints to drive projects forward while staying actively involved in development and deployment. 
Communicative and speaking four languages, I thrive in collaborative environments. 
Open to remote work with readiness for business trips when needed.
                    `

const languages_table = [
  ["Russian:", "native"],
  ["English:", "advanced"],
  ["German:",  "intermediate"],
];

const personal_info_table = [
  ["Name:", "Vasilii Tulskii"],
  ["Date of birth:", "12 March 1992"],
  ["City of residence:", "Frankfurt, Germany"],
  ["E-mail:", "tulskyva@gmail.com"],
];

const profile_table = [
  {icon: 'linkedin.png', 
    portal: "LinkedIn:", 
    href: 'https://www.linkedin.com/in/Vasily-Tulsky', 
    text: "Vasily-Tulsky"},

  {icon: 'gitlab.png',   
    portal: "GitLab:",   
    href: 'https://gitlab.com/tulskyva', 
    text: "TulskyVA"},

  {icon: 'scholar.png',  
    portal: "Scholar:",  
    href: 'https://scholar.google.ru/citations?user=McTmOxIAAAAJ', 
    text: "Vasily Tulsky"},
];
