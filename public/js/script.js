$(document).ready(function(){

  $("#about-me").ready(function()
  {
    $("#about-me").html(about_me.replace(/\n/g, "</br>"));
  });
  
  $("#photo").ready(function()
  {
    $("#photo").attr("src", `images/${card_arr[0].src}`);
  });

  $("#button-swap-next-photo").click(function()
  {
    var current_image_src = $("#photo").attr('src');
    var index = card_arr.findIndex(x => x.src === current_image_src.replace ("images/", ""));
    var next = card_arr[(index + 1) % card_arr.length];
    $("#photo").attr("src", `images/${next.src}`);
    
  });


  function create_table(object, data) {
    for(var i=0; i < data.length; i++)
      {
        var line = data[i];
        var tr = $('<tr />');
        for(var j=0; j < line.length; j++)
        {
            tr.append( $('<td />', {html : line[j]}) );
        };
      
        object.append(tr);
      };
  };

  $("#personal-info-table").ready(
    create_table($("#personal-info-table"), personal_info_table)
  );

  $("#languages-table").ready(
    create_table($("#languages-table"), languages_table)
  );

  $("#profile-table").ready(function()
  {
    for(var i=0; i < profile_table.length; i++)
    {
      var line = profile_table[i];
      var tr = $('<tr />');
      tr.append( $('<td />', {
                              html: `<span class="is-inline-flex" style="align-items: flex-end;">
                                    <img class="image is-16x16" style="margin-right: 5px;" src="images/icons/${line.icon}">
                                    ${line.portal}
                                </span>` 
                              }) );
      tr.append( $('<td />', {html : `<a href="${line.href}" target="_blank">${line.text}</a>`}) );
    
      $("#profile-table").append(tr);
    };
  });

});
